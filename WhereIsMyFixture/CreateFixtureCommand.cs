﻿using System;
using System.ComponentModel.Design;
using System.IO;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Task = System.Threading.Tasks.Task;

namespace WhereIsMyFixture
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class CreateFixtureCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        private const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        private static readonly Guid CommandSet = new Guid("c581d040-b5a4-4fe7-9ebb-9f305484dac0");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly AsyncPackage _package;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateFixtureCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private CreateFixtureCommand(AsyncPackage package, OleMenuCommandService commandService)
        {
            this._package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new MenuCommand(this.Execute, menuCommandID);
            commandService.AddCommand(menuItem);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        private static CreateFixtureCommand Instance { get; set; }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider => _package;

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(AsyncPackage package)
        {
            // Switch to the main thread - the call to AddCommand in CreateFixtureCommand's constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            OleMenuCommandService commandService = await package.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Instance = new CreateFixtureCommand(package, commandService);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            DTE dte = (DTE) Package.GetGlobalService(typeof(SDTE));

            var project = GetActiveProject(dte);

            var currentPath = dte.ActiveDocument.FullName;
            if (!CanFixtureBeCreated(_package, currentPath)) return;
            
            CreateDirectoryIfNecessary(currentPath, project);
            CreateFileIfNecessary(currentPath, project, _package);
        }

        private static Project GetActiveProject(_DTE dte)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            return dte.ActiveDocument.ProjectItem.ContainingProject;
        }

        private static void CreateDirectoryIfNecessary(string classPath, Project project)
        {
            if (PathsHelper.DoesNUnitDirectoryExist(classPath)) return;
            ThreadHelper.ThrowIfNotOnUIThread();

            string directoryPath = PathsHelper.GetNUnitDirectoryPath(classPath);
            Directory.CreateDirectory(directoryPath);

            project.ProjectItems.AddFolder(directoryPath);
        }

        private static void CreateFileIfNecessary(string classPath, Project project, AsyncPackage package)
        {
            if (PathsHelper.DoesFixtureExist(classPath)) return;
            ThreadHelper.ThrowIfNotOnUIThread();

            string fixtureFilePath = PathsHelper.GetFixturePath(classPath);
            File.WriteAllText(fixtureFilePath, FixtureTemplateHelper.GetFixtureClassTemplate(classPath));

            project.ProjectItems.AddFromFile(fixtureFilePath);
            ShowMessage(package, classPath);
        }

        private static void ShowMessage(IServiceProvider package, string classPath)
        {
            const string title = "TestFixture Successfully Created";
            string message = $"The TestFixture for {PathsHelper.GetClassName(classPath)} has been created at:" +
                             $"\n{PathsHelper.GetFixturePath(classPath)}";

            VsShellUtilities.ShowMessageBox(
                package,
                message,
                title,
                OLEMSGICON.OLEMSGICON_INFO,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }

        private static bool CanFixtureBeCreated(IServiceProvider package, string classPath)
        {
            if (!classPath.EndsWith(".cs"))
            {
                VsShellUtilities.ShowMessageBox(
                    package,
                    $"{classPath} does not represent a C# file and therefore no Fixture can be created.",
                    "File must be a .cs",
                    OLEMSGICON.OLEMSGICON_INFO,
                    OLEMSGBUTTON.OLEMSGBUTTON_OK,
                    OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                return false;
            }

            if (classPath.EndsWith("Fixture.cs"))
            {
                VsShellUtilities.ShowMessageBox(
                    package,
                    $"{classPath} is already fixture class a no fixtures can be created from this.",
                    "Class cannot be a fixture",
                    OLEMSGICON.OLEMSGICON_INFO,
                    OLEMSGBUTTON.OLEMSGBUTTON_OK,
                    OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                return false;
            }

            return true;
        }
    }
}