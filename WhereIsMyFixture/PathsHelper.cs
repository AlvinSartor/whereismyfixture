﻿using System.IO;

namespace WhereIsMyFixture
{
    public static class PathsHelper
    {
        /// <summary>
        /// Gets the name of the class.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static string GetClassName(string classPath) => 
            Path.GetFileNameWithoutExtension(classPath);

        /// <summary>
        /// Gets the name of the fixture file.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static string GetFixtureName(string classPath) =>
            GetClassName(classPath) + "Fixture.cs";

        /// <summary>
        /// Gets the fixture file path.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static string GetFixturePath(string classPath) => 
            Path.Combine(GetNUnitDirectoryPath(classPath), GetFixtureName(classPath));

        /// <summary>
        /// Gets the directory where the Fixture will be located.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static string GetNUnitDirectoryPath(string classPath) => 
            Path.Combine(Path.GetDirectoryName(classPath), "NUnit");

        /// <summary>
        /// Checks whether the Fixture folder already exists.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static bool DoesNUnitDirectoryExist(string classPath) =>
            Directory.Exists(GetNUnitDirectoryPath(classPath));

        /// <summary>
        /// Checks whether the Fixture file already exists.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static bool DoesFixtureExist(string classPath) =>
            DoesNUnitDirectoryExist(classPath) && File.Exists(GetFixturePath(classPath));
    }
}
